package net.chenlin.dp.ids.server.manager.impl;

import net.chenlin.dp.ids.common.constant.IdsConst;
import net.chenlin.dp.ids.common.entity.SessionData;
import net.chenlin.dp.ids.common.util.JsonUtil;
import net.chenlin.dp.ids.server.manager.SessionManager;
import net.chenlin.dp.ids.server.util.RedisUtil;
import org.springframework.stereotype.Component;

/**
 * session管理器
 * @author zcl<yczclcn@163.com>
 */
@Component("sessionManager")
public class SessionManagerImpl implements SessionManager {

    /**
     * 更新session过期时间
     * @param sessionId
     * @param loginType
     */
    @Override
    public void update(String sessionId, Integer loginType) {
        // 没有使用记住我设置，访问时刷新 manager redis过期时间
        String redisKey = sessionKey(sessionId, loginType);
        RedisUtil.expire(redisKey, IdsConst.REDIS_EXPIRE_TIME);

    }

    /**
     * 获取sessionData
     * @param sessionId
     * @return
     */
    @Override
    public SessionData get(String sessionId, Integer loginType) {
        String redisKey = sessionKey(sessionId, loginType);
        String json = RedisUtil.get(redisKey);
        return JsonUtil.toObj(json, SessionData.class);
    }

    /**
     * 保存session
     * @param sessionId
     * @param sessionData
     */
    @Override
    public void save(String sessionId, SessionData sessionData) {
        String redisKey = sessionKey(sessionId, sessionData.getLoginType());
        if (sessionData.getRememberMe()) {
            // cookie默认过期时间
            RedisUtil.set(redisKey, JsonUtil.toStr(sessionData), IdsConst.COOKIE_MAX_AGE);
        } else {
            // redis默认过期时间
            RedisUtil.set(redisKey, JsonUtil.toStr(sessionData));
        }
    }

    /**
     * 删除session
     * @param sessionId
     */
    @Override
    public void remove(String sessionId, Integer loginType) {
        String redisKey = sessionKey(sessionId, loginType);
        RedisUtil.del(redisKey);
    }

    /**
     * sessionKey
     * @param sessionId
     * @return
     */
    private String sessionKey(String sessionId, Integer loginType) {
        StringBuilder sb = new StringBuilder(IdsConst.REDIS_SESSION_KEY);
        if (IdsConst.LOGIN_TYPE_WEB == loginType) {
            sb.append("web:");
        }
        if (IdsConst.LOGIN_TYPE_APP == loginType) {
            sb.append("app:");
        }
        sb.append("session:").append(sessionId);
        return sb.toString();
    }

}
