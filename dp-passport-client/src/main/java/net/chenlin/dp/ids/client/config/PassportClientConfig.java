package net.chenlin.dp.ids.client.config;

import net.chenlin.dp.ids.common.constant.IdsConst;
import net.chenlin.dp.ids.common.util.CommonUtil;
import net.chenlin.dp.ids.common.util.WebUtil;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * passport客户端配置
 * @author zcl<yczclcn@163.com>
 */
@Configuration
@ConfigurationProperties(prefix = PassportClientConfig.PREFIX)
public class PassportClientConfig {

    static final String PREFIX = "ids.client";

    /**
     * 接入系统首页
     */
    private String serverName;

    /**
     * 接入应用标识
     */
    private String appCode;

    /**
     * 接入应用类型：web（默认） 或 app
     */
    private String clientType;

    /**
     * ids服务地址
     */
    private String authServerUrl;

    /**
     * authIdCookie名称
     */
    private String authIdCookieName;

    /**
     * authIdCookie的域
     */
    private String cookieDomain;

    /**
     * constructor
     */
    public PassportClientConfig() {
    }

    /**
     * getter for serverName
     * @return
     */
    public String getServerName() {
        return serverName;
    }

    /**
     * setter for serverName
     * @param serverName
     */
    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    /**
     * getter for appCode
     * @return
     */
    public String getAppCode() {
        return appCode;
    }

    /**
     * setter for appCode
     * @param appCode
     */
    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    /**
     * getter for clientType
     * @return
     */
    public String getClientType() {
        return clientType;
    }

    /**
     * setter for clientType
     * @param clientType
     */
    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    /**
     * getter for authServerUrl
     * @return
     */
    public String getAuthServerUrl() {
        return authServerUrl;
    }

    /**
     * setter for authServerUrl
     * @param authServerUrl
     */
    public void setAuthServerUrl(String authServerUrl) {
        this.authServerUrl = authServerUrl;
    }

    /**
     * getter for authIdCookieName
     * @return
     */
    public String getAuthIdCookieName() {
        return authIdCookieName;
    }

    /**
     * setter for authIdCookieName
     * @param authIdCookieName
     */
    public void setAuthIdCookieName(String authIdCookieName) {
        this.authIdCookieName = authIdCookieName;
    }

    /**
     * getter for cookieDomain
     * @return
     */
    public String getCookieDomain() {
        return cookieDomain;
    }

    /**
     * setter for cookieDomain
     * @param cookieDomain
     */
    public void setCookieDomain(String cookieDomain) {
        this.cookieDomain = cookieDomain;
    }

    /**
     * 拼接登录重定向地址
     * @param redirectUrl
     * @return
     */
    public String getServLoginUrl(String redirectUrl) {
        String baseUrl = this.authServerUrl + IdsConst.LOGIN_URL;
        if (CommonUtil.strIsEmpty(redirectUrl)) {
            redirectUrl = this.getServerName();
        }
        return WebUtil.requestAppendParam(baseUrl, new String[]{IdsConst.REDIRECT_KEY}, new Object[]{redirectUrl});
    }

    /**
     * 登出地址拼接
     * @return
     */
    public String getServLogoutUrl() {
        return this.authServerUrl.concat(IdsConst.LOGOUT_URL);
    }

    /**
     * 服务端session校验地址
     * @return
     */
    public String getServAuthStatusUrl() {
        return this.authServerUrl.concat("/client").concat(IdsConst.AUTH_STATUS_URL);
    }

    /**
     * 服务端session刷新地址
     * @return
     */
    public String getServAuthRefreshUrl() {
        return this.authServerUrl.concat("/client").concat(IdsConst.AUTH_REFRESH_URL);
    }

    /**
     * 服务端session删除地址
     * @return
     */
    public String getServAuthRemoveUrl() {
        return this.authServerUrl.concat("/client").concat(IdsConst.AUTH_REMOVE_URL);
    }

    /**
     * to string
     * @return
     */
    @Override
    public String toString() {
        return "PassportClientConfig{" +
                "serverName='" + serverName + '\'' +
                ", appCode='" + appCode + '\'' +
                ", clientType='" + clientType + '\'' +
                ", authServerUrl='" + authServerUrl + '\'' +
                ", authIdCookieName='" + authIdCookieName + '\'' +
                ", cookieDomain='" + cookieDomain + '\'' +
                '}';
    }

}
